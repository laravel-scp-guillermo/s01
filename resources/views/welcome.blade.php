@extends('layouts.app')

@section('content')
<div class="text-center mb-5">
    <img src="https://asset.brandfetch.io/ide68-31CH/idlxAUbIOo.jpeg" alt="" style="border-radius: 50%;width:150px">
    <h2>Laravel</h2>
</div>

<h3>Featured Post</h3>
@foreach($posts as $post)

<div class="card text-center m-2">
    <div class="card-body">
        <h4 class="card-title mb-3">
            <a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
  
    </div>
</div>
@endforeach

@endsection